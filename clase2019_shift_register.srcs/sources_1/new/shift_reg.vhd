----------------------------------------------------------------------------------
-- JORGE SOTO
-- Programa para demostrar el uso de registros de desplazamiento del FPGA
-- 32 x 8-bit compact shift register. Uses 12 LUTs (32 x 1-bit each), 3 slices
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_arith.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;

entity SHIFT_REG is
	Port ( 
		CLK  : in  STD_LOGIC;
		WE   : in  STD_LOGIC;
		DIN  : in  STD_LOGIC_VECTOR (7 downto 0);
		SEL  : in  STD_LOGIC_VECTOR (4 downto 0);
		DOUT : out STD_LOGIC_VECTOR (7 downto 0));
end SHIFT_REG ;

architecture Behavioral of SHIFT_REG is

type SHIFT_TYPE is array(31 downto 0) of std_logic_vector(DIN'range);
signal SHIFTR : SHIFT_TYPE := (
0 => x"4F",   -- Inicialización de valores
1 => x"D1",
others => x"11");

begin

process(CLK) begin
	if rising_edge(CLK) then
		if WE = '1' then
			SHIFTR <= SHIFTR(30 downto 0) & DIN; -- Dato de entrada a posición 0 
		end if;										-- d(n) = d(n-1)
		DOUT <= SHIFTR(conv_integer(SEL));		-- La salida puede ser cualquier posición
	end if;
end process;

end Behavioral;
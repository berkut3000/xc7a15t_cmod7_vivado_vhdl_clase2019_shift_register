--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
  
ENTITY TB_SHIFT_REG IS
END TB_SHIFT_REG;
 
ARCHITECTURE behavior OF TB_SHIFT_REG IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT SHIFT_REG
	Port ( 
        CLK  : in  STD_LOGIC;
        WE   : in  STD_LOGIC;
        DIN  : in  STD_LOGIC_VECTOR (7 downto 0);
        SEL  : in  STD_LOGIC_VECTOR (4 downto 0);
        DOUT : out STD_LOGIC_VECTOR (7 downto 0));
    END COMPONENT;
    

   --Inputs
   signal CLK : std_logic := '0';
   signal WE  : std_logic := '0';
   signal DIN : std_logic_vector(7 downto 0) := (others => '0');
   signal SEL : std_logic_vector(4 downto 0) := (others => '0');

 	--Outputs
   signal DOUT : std_logic_vector(7 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
   constant CLK_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: SHIFT_REG PORT MAP (
          CLK => CLK,
          WE  => WE,
          DIN => DIN,
          SEL => SEL,
          DOUT=> DOUT
        );

   -- Clock process definitions
   CLK_process :process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      SEL <= "00000";
      wait for 100 ns;	
      SEL <= "00001";
      wait for 100 ns;
      SEL <= "00010";
      DIN <= x"FF";
      wait for 100 ns;

      WE <= '1';
      wait for 20 ns;
      WE <= '0';
      wait for 100 ns;

      SEL <= "00000";
      wait for 100 ns;	
      SEL <= "00001";
      wait for 100 ns;
      SEL <= "00010";

      
      wait;
   end process;

END;
